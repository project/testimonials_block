<?php

namespace Drupal\testimonials_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Testimonials Block.
 *
 * @Block(
 *   id="testimonials_block",
 *   admin_label = @Translation("Testimonials Block"),
 * )
 */
class TestimonialsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The file storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructor for the custom plugin.
   *
   * Initializes the plugin with the given configuration, plugin ID,
   * and plugin definition.
   * Also injects the file storage and file URL generator services
   * for use within the plugin.
   *
   * @param array $configuration
   *   The configuration array for the plugin.
   * @param string $plugin_id
   *   The unique ID for the plugin.
   * @param array $plugin_definition
   *   The plugin definition containing metadata.
   * @param \Drupal\Core\Entity\EntityStorageInterface $file_storage
   *   The service used for file storage operations.
   * @param \Drupal\Core\Url\FileUrlGeneratorInterface $file_url_generator
   *   The service used to generate file URLs.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $file_storage, FileUrlGeneratorInterface $file_url_generator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileStorage = $file_storage;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'testimonials_block' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $items = [];
    if (isset($config['testimonials_data'])) {
      $items = $config['testimonials_data'];
      if ($items != '') {
        $items = json_decode($items);
      }
    }

    $form['testimonial_header'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Testimonial header content'),
      '#description' => $this->t('Testimonial header content'),
      '#default_value' => $config['testimonial_header'] ?? '',
      '#format' => $config['testimonial_header_format'] ?? 'basic_html',
    ];

    $form['#tree'] = TRUE;

    $form['items_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Testimonials'),
      '#prefix' => '<div id="items-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    if (!$form_state->has('num_items')) {
      if (is_array($items)) {
        $count_items = count($items);
      }
      else {
        $count_items = 0;
      }
      $form_state->set('num_items', $count_items);
    }
    $number_of_items = $form_state->get('num_items');

    for ($i = 0; $i < $number_of_items; $i++) {

      $is_active_row = $form_state->get("row_" . $i);

      if ($is_active_row != 'inactive') {

        $j = $i + 1;
        $form['items_fieldset']['items'][$i] = [
          '#type' => 'details',
          '#title' => $this->t('Testimonial @j', ['@j' => $j]),
          '#prefix' => '<div id="items-fieldset-wrapper">',
          '#suffix' => '</div>',
          '#open' => TRUE,
        ];
        $form['items_fieldset']['items'][$i]['author_image'] = [
          '#title' => $this->t('Author image'),
          '#type' => 'managed_file',
          '#upload_location' => 'public://module-images/home-slider-images/',
          '#multiple' => FALSE,
          '#description' => $this->t('Allowed extensions: gif png jpg jpeg.<br>The recommended aspect ratio of an image size is 1:1'),
          '#upload_validators' => [
            'file_validate_is_image' => [],
            'file_validate_extensions' => ['gif png jpg jpeg'],
            'file_validate_size' => [25600000],
          ],
          '#theme' => 'image_widget',
          '#preview_image_style' => 'medium',
          '#default_value' => $items[$i]->author_image ? [$items[$i]->author_image[0]] : NULL,
        ];
        $form['items_fieldset']['items'][$i]['quote'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Quote'),
          '#description' => $this->t('Testimonial content'),
          '#default_value' => $items[$i]->quote ?? '',
        ];
        $form['items_fieldset']['items'][$i]['author'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Author name'),
          '#description' => $this->t('Author name'),
          '#default_value' => $items[$i]->author ?? '',
        ];
        $form['items_fieldset']['items'][$i]['designation'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Designation'),
          '#description' => $this->t('Author designation'),
          '#default_value' => $items[$i]->designation ?? '',
        ];
        $form['items_fieldset']['items'][$i]['other_info'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Other info'),
          '#description' => $this->t('Author other extra information data'),
          '#default_value' => $items[$i]->other_info ?? '',
        ];
        $form['items_fieldset']['items'][$i]['weight'] = [
          '#type' => 'number',
          '#min' => 1,
          '#max' => 120,
          '#title' => $this->t('Testimonial order weight'),
          '#description' => $this->t('The weight field can be used to provide customized sorting of testimonials.'),
          '#default_value' => $items[$i]->weight ?? $j,
        ];
        $form['items_fieldset']['items'][$i]['remove_single_item'] = [
          '#type' => 'submit',
          '#value' => $this->t('Remove item'),
          '#name' => 'row_' . $i,
          '#submit' => [[$this, 'removeItemCallback']],
          '#ajax' => [
            'callback' => [$this, 'addmoreCallback'],
            'wrapper' => 'items-fieldset-wrapper',
          ],
          '#button_type' => 'danger',
        ];

      }
    }

    $form['items_fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    $form['items_fieldset']['actions']['add_item'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add testimonial'),
      '#submit' => [[$this, 'addOne']],
      '#ajax' => [
    // '\Drupal\testimonials\Plugin\Block\ALaUneBlock::addmoreCallback',
        'callback' => [$this, 'addmoreCallback'],
        'wrapper' => 'items-fieldset-wrapper',
      ],
      '#button_type' => 'primary',
    ];

    $form['carousel_configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Carousel configuration'),
      '#description' => $this->t('Make adjustments such as how many items will appear according to screen sizes and other configurations.'),
      '#prefix' => '<div id="items-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['carousel_configuration']['extra_small'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of items for extra small device.'),
      '#description' => $this->t('Breakpoint from 0 up. Eg. mobile device'),
      '#options' => [
        '1' => '1',
      ],
      '#default_value' => $config['small'] ?? '1',
    ];

    $form['carousel_configuration']['small'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of items for small device'),
      '#description' => $this->t('Breakpoint from 768 up. Eg. mobile portrait mode'),
      '#options' => [
        '1' => '1',
        '2' => '2',
      ],
      '#default_value' => $config['small'] ?? '1',
    ];

    $form['carousel_configuration']['medium'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of items for medium device'),
      '#description' => $this->t('Breakpoint from 992 up. Eg. ipad-device'),
      '#options' => [
        '1' => '1',
        '2' => '2',
        '3' => '3',
      ],
      '#default_value' => $config['medium'] ?? '2',
    ];

    $form['carousel_configuration']['large'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of items for large device'),
      '#description' => $this->t('Breakpoint from 1200 up. Eg. desktop and large screen'),
      '#options' => [
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
      ],
      '#default_value' => $config['large'] ?? '3',
    ];

    $form['carousel_configuration']['extra_small_nav'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show nav icons for extra small devices'),
      '#description' => $this->t('Breakpoint from 0 up. Eg. mobile device'),
      '#default_value' => $config['extra_small_nav'] ?? FALSE,
    ];

    $form['carousel_configuration']['small_nav'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show nav icons for small device'),
      '#description' => $this->t('Breakpoint from 768 up. Eg. mobile portrait mode'),
      '#default_value' => $config['small_nav'] ?? FALSE,
    ];

    $form['carousel_configuration']['medium_nav'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show nav icons for medium device'),
      '#description' => $this->t('Breakpoint from 992 up. Eg. ipad-device'),
      '#default_value' => $config['medium_nav'] ?? TRUE,
    ];

    $form['carousel_configuration']['large_nav'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show nav icons for large device'),
      '#description' => $this->t('Breakpoint from 1200 up. Eg. desktop and large screen'),
      '#default_value' => $config['large_nav'] ?? TRUE,
    ];

    $form['carousel_configuration']['extra_small_dots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show dots for extra small device.'),
      '#description' => $this->t('Breakpoint from 0 up. Eg. mobile device'),
      '#default_value' => $config['extra_small_dots'] ?? FALSE,
    ];

    $form['carousel_configuration']['small_dots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show dots for small device'),
      '#description' => $this->t('Breakpoint from 768 up. Eg. mobile portrait mode'),
      '#default_value' => $config['small_dots'] ?? FALSE,
    ];

    $form['carousel_configuration']['medium_dots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show dots for medium device'),
      '#description' => $this->t('Breakpoint from 992 up. Eg. ipad-device'),
      '#default_value' => $config['medium_dots'] ?? TRUE,
    ];

    $form['carousel_configuration']['large_dots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show dots for large device'),
      '#description' => $this->t('Breakpoint from 1200 up. Eg. desktop and large screen'),
      '#default_value' => $config['large_dots'] ?? TRUE,
    ];
    $form['carousel_configuration']['hide_box_shadow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide box shadow'),
      '#description' => $this->t('Hide box shadow for the items'),
      '#default_value' => $config['hide_box_shadow'] ?? FALSE,
    ];

    return $form;
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeItemCallback(array &$form, FormStateInterface $form_state) {
    $button_clicked = $form_state->getTriggeringElement()['#name'];

    $form_state->set($button_clicked, 'inactive');

    $form_state->setRebuild();

  }

  /**
   * Adds one to the number of items stored in the form state.
   *
   * This function increments the 'num_items' value in the form state by one
   * and then triggers a form rebuild to reflect the updated value.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $number_of_items = $form_state->get('num_items');
    $add_button = $number_of_items + 1;
    $form_state->set('num_items', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Callback function for adding more items in the form.
   *
   * This function is triggered when an AJAX callback is invoked, returning
   * the 'items_fieldset' element from the form, which is located under the
   * 'settings' form section. This allows dynamic updates of the form's fieldset
   * during an AJAX request.
   *
   * @param array $form
   *   The entire form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return mixed
   *   The form element to be returned and re-rendered.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    // The form passed here is the entire form, not the subform that is
    // passed to non-AJAX callback.
    return $form['settings']['items_fieldset'];
  }

  /**
   * Callback function for removing an item from the form.
   *
   * This function is triggered when an AJAX callback is invoked to remove
   * an item. It decreases the 'num_items' value in the form state by one,
   * ensuring that the number of items never goes below one. After updating
   * the form state, a rebuild is triggered to reflect the change.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $number_of_items = $form_state->get('num_items');
    if ($number_of_items > 1) {
      $remove_button = $number_of_items - 1;
      $form_state->set('num_items', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * Callback function for removing a single item from the form.
   *
   * This function is triggered when an AJAX callback is invoked to remove
   * a single item. It decreases the 'num_items' value in the form state by one,
   * ensuring that the number of items never goes below one. After updating
   * the form state, a rebuild is triggered to reflect the change.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function removeCallbackSingle(array &$form, FormStateInterface $form_state) {
    $number_of_items = $form_state->get('num_items');
    if ($number_of_items > 1) {
      $remove_button = $number_of_items - 1;
      $form_state->set('num_items', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $testimonials_data_config = $this->configuration['testimonials_data'] ?? '';
    $testimonials_block_old_image_ids = testimonials_block_old_image_ids($testimonials_data_config);
    $current_image_ids = [];
    $items = [];

    $values = $form_state->getValues();
    foreach ($values as $key => $value) {

      if ($key === 'items_fieldset') {
        if (isset($value['items']) && !empty($value['items'])) {
          $items = $value['items'];

          usort($items, function ($x, $y) {
            if (is_numeric($x['weight']) && is_numeric($y['weight'])) {
              return $x['weight'] - $y['weight'];
            }
          });

          foreach ($items as $key => $item) {
            if (trim($item['quote']) === '') {
              unset($items[$key]);
            }
            else {
              if (!is_numeric($item['weight'])) {
                $items[$key]['weight'] = 1;
              }

              if (isset($item['author_image'][0])) {
                $image_id = $item['author_image'][0];
                $file = $this->fileStorage->load($image_id);
                $file->setPermanent();
                $file->save();
                $current_image_ids[] = $image_id;
              }
            }
          }

          $testimonials_data = array_values($items);
          $testimonials_data = json_encode($testimonials_data);

          $this->configuration['testimonials_data'] = $testimonials_data;
        }
        else {
          $this->configuration['testimonials_data'] = '';
        }

        // Remove old images.
        $result = array_diff($testimonials_block_old_image_ids, $current_image_ids);
        if (!empty($result)) {
          foreach ($result as $key => $author_image_id) {
            $file = $this->fileStorage->load($author_image_id);
            $file->setTemporary();
            $file->save();
          }
        }
      }
    }

    if ($form_state->getValue('carousel_configuration')) {
      $carousel_configuration = $form_state->getValue('carousel_configuration');
      $this->configuration['small'] = $carousel_configuration['small'];
      $this->configuration['medium'] = $carousel_configuration['medium'];
      $this->configuration['large'] = $carousel_configuration['large'];

      $this->configuration['extra_small_nav'] = $carousel_configuration['extra_small_nav'];
      $this->configuration['small_nav'] = $carousel_configuration['small_nav'];
      $this->configuration['medium_nav'] = $carousel_configuration['medium_nav'];
      $this->configuration['large_nav'] = $carousel_configuration['large_nav'];

      $this->configuration['extra_small_dots'] = $carousel_configuration['extra_small_dots'];
      $this->configuration['small_dots'] = $carousel_configuration['small_dots'];
      $this->configuration['medium_dots'] = $carousel_configuration['medium_dots'];
      $this->configuration['large_dots'] = $carousel_configuration['large_dots'];
      $this->configuration['hide_box_shadow'] = $carousel_configuration['hide_box_shadow'];
    }

    $this->configuration['testimonial_header'] = $values['testimonial_header']['value'];
    $this->configuration['testimonial_header_format'] = $values['testimonial_header']['format'];

  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $testimonials_data_config = $this->configuration['testimonials_data'] ?? '';
    $small = $this->configuration['small'] ?? '1';
    $medium = $this->configuration['medium'] ?? '2';
    $large = $this->configuration['large'] ?? '2';

    $extra_small_nav = ($this->configuration['extra_small_nav'] == 1) ? TRUE : FALSE;
    $small_nav = ($this->configuration['small_nav'] == 1) ? TRUE : FALSE;
    $medium_nav = ($this->configuration['medium_nav'] == 1) ? TRUE : FALSE;
    $large_nav = ($this->configuration['large_nav'] == 1) ? TRUE : FALSE;

    $extra_small_dots = ($this->configuration['extra_small_dots'] == 1) ? TRUE : FALSE;
    $small_dots = ($this->configuration['small_dots'] == 1) ? TRUE : FALSE;
    $medium_dots = ($this->configuration['medium_dots'] == 1) ? TRUE : FALSE;
    $large_dots = ($this->configuration['large_dots'] == 1) ? TRUE : FALSE;
    $hide_box_shadow = ($this->configuration['hide_box_shadow'] == 1) ? TRUE : FALSE;

    $testimonial_header = $this->configuration['testimonial_header'] ?? '';

    $responsive_settings = [
      'small' => $small,
      'medium' => $medium,
      'large' => $large,
      'extra_small_nav' => $extra_small_nav,
      'small_nav' => $small_nav,
      'medium_nav' => $medium_nav,
      'large_nav' => $large_nav,
      'extra_small_dots' => $extra_small_dots,
      'small_dots' => $small_dots,
      'medium_dots' => $medium_dots,
      'large_dots' => $large_dots,
      'hide_box_shadow' => $hide_box_shadow,
    ];

    $testimonials_data = [];
    if ($testimonials_data_config != '') {
      $testimonials_data = json_decode($testimonials_data_config);

      foreach ($testimonials_data as $key => $testimonial) {
        $image_url = '';
        $author_image_id = $testimonial->author_image[0] ?? '';
        if ($author_image_id != '') {
          $file = $this->fileStorage->load($author_image_id);
          if ($file) {
            $image_url = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
          }
        }
        $testimonials_data[$key]->image_url = $image_url;
      }
    }
    $build = [];
    $build['testimonials'] = [
      '#theme' => 'testimonials_block',
      '#testimonials_data' => $testimonials_data,
      '#responsive_settings' => $responsive_settings,
      '#testimonial_header' => $testimonial_header,
    ];
    $build['#attached']['library'][] = 'testimonials_block/testimonials_block';
    return $build;
  }

}
