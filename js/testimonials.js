(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.testimonials = {
        attach: function (context, settings) {
            const $context = $(context);
            $(once('testimonials', $context.find('.as-carousel'), context)).each(function () {
                $('.as-carousel').each(function () {
                    var owlCarousel = $(this),
                    loop = owlCarousel.data('loop'),
                    items = owlCarousel.data('items'),
                    margin = owlCarousel.data('margin'),
                    stagePadding = owlCarousel.data('stage-padding'),
                    autoplay = owlCarousel.data('autoplay'),
                    autoplayTimeout = owlCarousel.data('autoplay-timeout'),
                    smartSpeed = owlCarousel.data('smart-speed'),
                    dots = owlCarousel.data('dots'),
                    nav = owlCarousel.data('nav'),
                    navSpeed = owlCarousel.data('nav-speed'),
                    xsDevice = owlCarousel.data('mobile-device'),
                    xsDeviceNav = owlCarousel.data('mobile-device-nav'),
                    xsDeviceDots = owlCarousel.data('mobile-device-dots'),
                    smDevice = owlCarousel.data('ipad-device'),
                    smDeviceNav = owlCarousel.data('ipad-device-nav'),
                    smDeviceDots = owlCarousel.data('ipad-device-dots'),
                    smDevice2 = owlCarousel.data('ipad-device-two'),
                    smDeviceNav2 = owlCarousel.data('ipad-device-nav-two'),
                    smDeviceDots2 = owlCarousel.data('ipad-device-dots-two'),
                    mdDevice = owlCarousel.data('md-device'),
                    centerMode = owlCarousel.data('center-mode'),
                    HoverPause = owlCarousel.data('hoverpause'),
                    mdDeviceNav = owlCarousel.data('md-device-nav'),
                    mdDeviceDots = owlCarousel.data('md-device-dots');
                    owlCarousel.owlCarousel({
                        autoHeight : 'TRUE',
                        loop: (loop ? 'TRUE' : 'FALSE'),
                        items: (items ? items : 3),
                        lazyLoad: 'TRUE',
                        center: (centerMode ? 'TRUE' : 'FALSE'),
                        autoplayHoverPause: (HoverPause ? 'TRUE' : 'FALSE'),
                        margin: (margin ? margin : 0),
                        autoplay: (autoplay ? 'TRUE' : 'FALSE'),
                        autoplayTimeout: (autoplayTimeout ? autoplayTimeout : 1000),
                        smartSpeed: (smartSpeed ? smartSpeed : 250),
                        dots: (dots ? 'TRUE' : 'FALSE'),
                        nav: (nav ? 'TRUE' : 'FALSE'),
                        navText: ["<span class='previous round'>&#8249;</span>", "<span class='next round'>&#8250;</span>"],
                        navSpeed: (navSpeed ? 'TRUE' : 'FALSE'),
                        responsiveClass: 'TRUE',
                        responsive: {
                            0: {
                                items: xsDevice,
                                nav: xsDeviceNav,
                                dots: xsDeviceDots,
                                center: 'FALSE',
                            },
                            768: {
                                items: smDevice2,
                                nav: smDeviceNav2,
                                dots: smDeviceDots2,
                                center: 'FALSE',
                            },
                            992: {
                                items: smDevice,
                                nav: smDeviceNav,
                                dots: smDeviceDots,
                                center: 'FALSE',
                            },
                            1200: {
                                items: mdDevice,
                                nav: mdDeviceNav,
                                dots: mdDeviceDots,
                            }
                        }
                    });
                });
            });
        }
    }
}(jQuery, Drupal, drupalSettings));
